/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant_System;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nouryasser
 */
@XmlRootElement(name = "dishes")
@XmlAccessorType(XmlAccessType.FIELD)
public class DishesXML {

    @XmlElement(name = "dish")
    private List<DishXML> disheslist;

    public List<DishXML> getDisheslist() {
        return disheslist;
    }

    public void setDisheslist(List<DishXML> disheslist) {
        this.disheslist = disheslist;
    }

}
