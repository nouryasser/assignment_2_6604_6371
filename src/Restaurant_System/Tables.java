/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant_System;

import Model.Table;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nouryasser
 */
@XmlRootElement(name = "tables")
@XmlAccessorType(XmlAccessType.FIELD)
public class Tables {

    @XmlElement(name = "table")
    private List<Table> tableslist;

    public List<Table> getTableslist() {
        return tableslist;
    }

    public void setTableslist(List<Table> tableslist) {
        this.tableslist = tableslist;
    }

}
