/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant_System;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nouryasser
 */
@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)
public class UsersXML {

    @XmlElement(name = "user")
    private List<UserXML> userslist;

    public List<UserXML> getUserslist() {
        return userslist;
    }

    public void setUserslist(List<UserXML> userslist) {
        this.userslist = userslist;
    }

}
