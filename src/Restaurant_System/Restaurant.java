/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Restaurant_System;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nouryasser
 */
@XmlRootElement(name = "restaurant")
@XmlAccessorType(XmlAccessType.FIELD)

public class Restaurant {

    @XmlElement(name = "users")
    private UsersXML users = null;
    private DishesXML dishes = null;
    private Tables tables = null;

    public UsersXML getUsers() {
        return users;
    }

    public void setUsers(UsersXML users) {
        this.users = users;
    }

    public DishesXML getDishes() {
        return dishes;
    }

    public void setDishes(DishesXML dishes) {
        this.dishes = dishes;
    }

    public Tables getTables() {
        return tables;
    }

    public void setTables(Tables tables) {
        this.tables = tables;
    }

}
