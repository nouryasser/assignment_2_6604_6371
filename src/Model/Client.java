/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import GUI.Order_Screen;
import GUI.Reservation;
import Restaurant_System.DishXML;
import Restaurant_System.Restaurant;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

/**
 *
 * @author nouryasser
 */
public class Client extends User {

    private Component rootPane;
    static int tablenum;

    public Client(String name, String username, String password) {
        super(name, username, password);
    }

    public static double calcPrice(String type, double price) {
        double newprice = 0;
        if (type.equals("Main Course")) {
            newprice = price * 1.15;
        }
        if (type.equals("Dessert")) {
            newprice = price * 1.2;
        }
        if (type.equals("Appetizer")) {
            newprice = price * 1.1;
        }
        return newprice;
    }

    public static void loadDishes(Restaurant myRestaurant, JTable jTable1) {
        List<DishXML> dishesXML = myRestaurant.getDishes().getDisheslist();
        DefaultTableModel m = (DefaultTableModel) jTable1.getModel();
        for (int k = 0; k < 7; k++) {
            m.addRow(new Object[]{dishesXML.get(k).getName(), dishesXML.get(k).getPrice()});
        }
        List<Dish> dishes = new ArrayList<>();
        for (DishXML d : dishesXML) {
            if (("main_course").equals(d.getType())) {
            } else {
                Dish mc = new Main_Course(d.getName(), d.getPrice());
                dishes.add(mc);
            }
            if (("appetizer").equals(d.getType())) {
                Dish a = new Appetizer(d.getName(), d.getPrice());
                dishes.add(a);
            }
            if (("desert").equals(d.getType())) {
                Dish de = new Dessert(d.getName(), d.getPrice());
                dishes.add(de);
            }
        }
    }

    public static void addClientName(String orderer, Restaurant myRestaurant) {
        DishXML dish = new DishXML();
        dish.setName("Client Name: " + orderer + " " + "Table Number: " + tablenum);
        try {
            Dish.saveDishes(myRestaurant, dish);
        } catch (JAXBException ex) {
            Logger.getLogger(Order_Screen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public double placingOrder(Restaurant myRestaurant, JComboBox jComboBox2, JTable jTable2) throws JAXBException {
        List<Dish> dishes = null;
        DefaultTableModel m = (DefaultTableModel) jTable2.getModel();
        double totalorder = 0;
        double totalprice = 0;
        DishXML dish = new DishXML();
        DishXML total = new DishXML();
        if (jComboBox2.getSelectedIndex() == 0) {
            Dish mc = new Main_Course("Grilled Chicken", 75);
            totalprice += calcPrice("Main Course", 75);
            m.addRow(new Object[]{mc.getName(), calcPrice("Main Course", 75)});
            dish.setName("Grilled Chicken");
            dish.setType("main_course");
            dish.setPrice(75);
        }
        if (jComboBox2.getSelectedIndex() == 1) {
            Dish a = new Appetizer("Greek Salad", 35);
            totalorder += calcPrice("Appetizer", 35);
            totalprice += totalorder;
            m.addRow(new Object[]{"Greek Salad", calcPrice("Appetizer", 35)});
            dish.setName("Greek Salad");
            dish.setType("appetizer");
            dish.setPrice(35);
        }
        if (jComboBox2.getSelectedIndex() == 2) {
            Dish a = new Appetizer("Fried Potatoes", 30);
            totalorder += calcPrice("Appetizer", 30);
            totalprice += totalorder;
            m.addRow(new Object[]{"Fried Potatoes", calcPrice("Appetizer", 30)});
            dish.setName("Fried Potatoes");
            dish.setType("appetizer");
            dish.setPrice(30);
        }
        if (jComboBox2.getSelectedIndex() == 3) {
            Dish de = new Dessert("Apple Pie", 50);
            totalorder += calcPrice("Dessert", 50);
            totalprice += totalorder;
            m.addRow(new Object[]{"Apple Pie", calcPrice("Dessert", 50)});
            dish.setName("Apple Pie");
            dish.setType("desert");
            dish.setPrice(50);
        }
        if (jComboBox2.getSelectedIndex() == 4) {
            Dish de = new Dessert("Molten Cake", 60);
            totalorder += calcPrice("Dessert", 60);
            totalprice += totalorder;
            m.addRow(new Object[]{"Molten Cake", calcPrice("Dessert", 60)});
            dish.setName("Molten Cake");
            dish.setType("desert");
            dish.setPrice(60);
        }
        if (jComboBox2.getSelectedIndex() == 5) {
            Dish mc = new Main_Course("Mushroom Soup", 60);
            totalorder += calcPrice("Main Course", 60);
            totalprice += totalorder;
            m.addRow(new Object[]{mc.getName(), calcPrice("Main Course", 60)});
            dish.setName("Mushroom Soup");
            dish.setType("main_course");
            dish.setPrice(60);
        }
        if (jComboBox2.getSelectedIndex() == 6) {
            Dish mc = new Main_Course("Beef Steak", 80);
            totalorder += calcPrice("Main Course", 80);
            totalprice += totalorder;
            m.addRow(new Object[]{mc.getName(), calcPrice("Main Course", 80)});
            dish.setName("Beef Steak");
            dish.setType("main_course");
            dish.setPrice(80);
        }
        try {
            Dish.saveDishes(myRestaurant, dish);
        } catch (JAXBException ex) {
            Logger.getLogger(Order_Screen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return totalprice;
    }

    public void reserveTable(Restaurant myRestaurant, JSpinner jSpinner1, JRadioButton jRadioButton2, boolean reserved[]) {
        Table table1 = new Table();
        int numofseats = Integer.parseInt(jSpinner1.getValue().toString());
        boolean smoking = jRadioButton2.isSelected();
        if (numofseats == 5 && smoking == false) {
            if (reserved[0] == false) {
                table1.setNumber(1);
                table1.setNumberOfSeats(5);
                table1.setSmoking(false);
                tablenum = 1;
                reserved[0] = true;
                JOptionPane.showMessageDialog(rootPane, "Reservation Successful");
            } else {
                JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
            }

        } else if (numofseats == 12 && smoking == false) {
            if (reserved[1] == false) {
                table1.setNumber(2);
                table1.setNumberOfSeats(12);
                table1.setSmoking(false);
                tablenum = 2;
                reserved[1] = true;
                JOptionPane.showMessageDialog(rootPane, "Reservation Successful");
            } else {
                JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
            }

        } else if (numofseats == 12 && smoking == true) {
            if (reserved[2] == false) {
                table1.setNumber(3);
                table1.setNumberOfSeats(12);
                table1.setSmoking(true);
                tablenum = 3;
                reserved[2] = true;
                JOptionPane.showMessageDialog(rootPane, "Reservation Successful");
            } else {
                JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
            }
        } else if (numofseats == 4 && smoking == false) {
            if (reserved[3] == false) {
                table1.setNumber(4);
                table1.setNumberOfSeats(4);
                table1.setSmoking(false);
                tablenum = 4;
                reserved[3] = true;
                JOptionPane.showMessageDialog(rootPane, "Reservation Successful");
            } else {
                JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
            }

        } else if (numofseats == 4 && smoking == true) {
            if (reserved[4] == false) {
                table1.setNumber(5);
                table1.setNumberOfSeats(4);
                table1.setSmoking(true);
                tablenum = 5;
                reserved[4] = true;
                JOptionPane.showMessageDialog(rootPane, "Reservation Successful");
            } else {
                JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
            }

        } else if (numofseats == 7 && smoking == true) {
            if (reserved[5] == false) {
                table1.setNumber(6);
                table1.setNumberOfSeats(7);
                table1.setSmoking(true);
                tablenum = 6;
                reserved[5] = true;
                JOptionPane.showMessageDialog(rootPane, "Reservation Successful");
            } else {
                JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
            }

        } else if (numofseats == 6 && smoking == true) {
            if (reserved[6] == false) {
                table1.setNumber(7);
                table1.setNumberOfSeats(6);
                table1.setSmoking(true);
                tablenum = 7;
                reserved[6] = true;
                JOptionPane.showMessageDialog(rootPane, "Reservation Successful");
            } else {
                JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "The Selected Table is Not Available");
        }
        try {
            Table.saveTables(myRestaurant, table1);
        } catch (JAXBException ex) {
            Logger.getLogger(Reservation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void loadAvaTables(Restaurant myRestaurant, JTable jTable1, boolean reserved[]) {
        List<Table> tableslist = myRestaurant.getTables().getTableslist();
        boolean found;
        DefaultTableModel m = (DefaultTableModel) jTable1.getModel();
        for (int i = 0; i < 7; i++) {
            if (reserved[i] == false) {
                m.addRow(new Object[]{tableslist.get(i).getNumber(), tableslist.get(i).getNumberOfSeats()});
                found = true;
            }
        }
        if (found = false) {
            JOptionPane.showMessageDialog(rootPane, "We're Sorry, There Are No Available Tables at the Moment");
        }

    }

}
