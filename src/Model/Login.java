/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import GUI.Client_Screen;
import GUI.Cook_Screen;
import GUI.Login_Screen;
import GUI.Manager_Screen;
import GUI.Waiter_Screen;
import Restaurant_System.Restaurant;
import Restaurant_System.UserXML;
import java.awt.Component;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.xml.bind.JAXBException;

/**
 *
 * @author nouryasser
 */
public class Login {

    private Component rootPane;
    public void loggingIn(Restaurant myRestaurant,JTextField jTextField1,JPasswordField jPasswordField1,String orderer,List<User> userslist){
        String username = jTextField1.getText();
        String password = jPasswordField1.getText();
       User myuser = User.Validate(userslist, username, password);
        if (myuser != null) {
            JOptionPane.showMessageDialog(rootPane, "Login Successful");
            if (myuser instanceof Manager) {
                Manager_Screen ms;
                try {
                    ms = new Manager_Screen(myRestaurant);
                    ms.setVisible(true);
                } catch (JAXBException ex) {
                    Logger.getLogger(Login_Screen.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (myuser instanceof Waiter) {
                Waiter_Screen ws = new Waiter_Screen(myRestaurant);
                ws.setVisible(true);
            } else if (myuser instanceof Client) {
                UserXML u = new UserXML();
                u.setName(myuser.getName());
                u.setUsername(myuser.getUsername());
                u.setPassword(myuser.getPassword());
                u.setRole("Client");
                
                try {
                    User.saveUsers(myRestaurant, u);
                } catch (JAXBException ex) {
                    Logger.getLogger(Login_Screen.class.getName()).log(Level.SEVERE, null, ex);
                }
                userslist.add(myuser);
                orderer = myuser.getName();
                Client_Screen cs = new Client_Screen(myRestaurant, orderer);
                cs.setVisible(true);
            } else if (myuser instanceof Cooker) {
                Cook_Screen cs;
                try {
                    cs = new Cook_Screen(myRestaurant);
                    cs.setVisible(true);
                } catch (JAXBException ex) {
                    Logger.getLogger(Login_Screen.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            JOptionPane.showMessageDialog(rootPane, "Invalid Username or Password");
        }
    }
}
