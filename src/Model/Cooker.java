/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Restaurant_System.DishXML;
import Restaurant_System.Restaurant;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author nouryasser
 */
public class Cooker extends User {

    public Cooker(String name, String username, String password) {
        super(name, username, password);
    }

    public static void displayOrders(Restaurant myRestaurant, JTable jTable1) {
        List<DishXML> orders = myRestaurant.getDishes().getDisheslist();
        for (int i = 7; i < orders.size(); i++) {
            if ("Grilled Chicken".equals(orders.get(i).getName()) || "Greek Salad".equals(orders.get(i).getName()) || "Apple Pie".equals(orders.get(i).getName()) || "Mushroom Soup".equals(orders.get(i).getName()) || "Molten Cake".equals(orders.get(i).getName()) || "Fried Potatoes".equals(orders.get(i).getName()) || "Beef Steak".equals(orders.get(i).getName())) {
                DefaultTableModel d = (DefaultTableModel) jTable1.getModel();
                d.addRow(new Object[]{orders.get(i).getName()});
            }
        }
    }

}
