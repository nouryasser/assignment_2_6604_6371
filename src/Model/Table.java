/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Restaurant_System.Restaurant;
import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author nouryasser
 */
@XmlRootElement(name = "table")
@XmlAccessorType(XmlAccessType.FIELD)
public class Table {

    @XmlElement(name = "number")
    private int number;
    @XmlElement(name = "number_of_seats")
    private int numberOfSeats;
    @XmlElement(name = "smoking")
    private boolean smoking;

    public static void saveTables(Restaurant myRestaurant, Table table) throws JAXBException {
        List<Table> tableslist = null;
        JAXBContext jax = JAXBContext.newInstance(Restaurant.class);
        Marshaller m = jax.createMarshaller();
        myRestaurant.getTables().getTableslist().add(table);
        m.marshal(myRestaurant, new File("restaurant.xml"));
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public boolean isSmoking() {
        return smoking;
    }

    public void setSmoking(boolean smoking) {
        this.smoking = smoking;
    }
}
