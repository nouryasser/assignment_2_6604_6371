/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Restaurant_System.Restaurant;
import Restaurant_System.UserXML;
import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author nouryasser
 */
public class XMLfile {

    public static Restaurant ReadXml() throws JAXBException {
        JAXBContext jax = JAXBContext.newInstance(Restaurant.class);
        Unmarshaller um = jax.createUnmarshaller();
        Restaurant myRestaurant = (Restaurant) um.unmarshal(new File("restaurant.xml"));
        return myRestaurant;
    }

    public static void ValidateUser(Restaurant myRestaurant, List<User> userslist) {
        List<UserXML> usersXML = myRestaurant.getUsers().getUserslist();
        for (UserXML u : usersXML) {
            if (u.getRole().equals("Manager")) {
                Manager m = new Manager(u.getName(), u.getUsername(), u.getPassword());
                userslist.add(m);
            }
            if (u.getRole().equals("Client")) {
                Client cl = new Client(u.getName(), u.getUsername(), u.getPassword());
                userslist.add(cl);
            }
            if (u.getRole().equals("Waiter")) {
                Waiter w = new Waiter(u.getName(), u.getUsername(), u.getPassword());
                userslist.add(w);
            }
            if (u.getRole().equals("Cooker")) {
                Cooker c = new Cooker(u.getName(), u.getUsername(), u.getPassword());
                userslist.add(c);
            }
        }
    }

}
