/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Restaurant_System.DishXML;
import Restaurant_System.Restaurant;
import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author nouryasser
 */
public class Dish {

    private String name;
    private double price;
    Restaurant myRestaurant;

    public Dish(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public static void saveDishes(Restaurant myRestaurant, DishXML dish) throws JAXBException {
        List<DishXML> disheslist;
        JAXBContext jax = JAXBContext.newInstance(Restaurant.class);
        Marshaller m = jax.createMarshaller();
        myRestaurant.getDishes().getDisheslist().add(dish);
        m.marshal(myRestaurant, new File("restaurant.xml"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
