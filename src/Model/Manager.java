/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import GUI.Order_Screen;
import Restaurant_System.DishXML;
import Restaurant_System.Restaurant;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.JAXBException;

/**
 *
 * @author nouryasser
 */
public class Manager extends User {

    public Manager(String name, String username, String password) {
        super(name, username, password);
    }

    public static void displayOrders(Restaurant myRestaurant, JTable jTable2) {
        List<DishXML> orders = myRestaurant.getDishes().getDisheslist();
        for (int i = 7; i < orders.size(); i++) {
            DefaultTableModel d = (DefaultTableModel) jTable2.getModel();
            if (orders.get(i) != null && orders.get(i).getName() != null && !orders.get(i).getName().contains("null") && !orders.get(i).getName().contains("0")) {
                d.addRow(new Object[]{orders.get(i).getName()});
            }
        }
    }

    public static void displayTotal(Restaurant myRestaurant, JLabel jLabel4, double totalprice) throws JAXBException {
        Order_Screen os = new Order_Screen(myRestaurant, null);
        jLabel4.setText("$" + totalprice);
    }
}
