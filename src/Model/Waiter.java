/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Restaurant_System.Restaurant;
import Restaurant_System.UserXML;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author nouryasser
 */
public class Waiter extends User {

    public Waiter(String name, String username, String password) {
        super(name, username, password);
    }

    public static void displayUsers(Restaurant myRestaurant, JTable jTable1) {
        List<UserXML> clients = myRestaurant.getUsers().getUserslist();
        List<Table> tables = myRestaurant.getTables().getTableslist();
        for (int i = 5; i <= 5 + count; i++) {
            if (tables.get(i + 2).getNumber() != 0)
            {
                DefaultTableModel d = (DefaultTableModel) jTable1.getModel();
                d.addRow(new Object[]{clients.get(i - 1).getName()});
            }
        }
    }
    static int count;

    public static void displayTablenos(Restaurant myRestaurant, JTable jTable2) {
        List<Table> tables = myRestaurant.getTables().getTableslist();
        count = 1;
        for (int i = 7; i < tables.size(); i++) {
            if (tables.get(i).getNumber() != 0) {

                count++;
                DefaultTableModel d = (DefaultTableModel) jTable2.getModel();
                d.addRow(new Object[]{tables.get(i).getNumber()});
                
            }
        }
    }

}
