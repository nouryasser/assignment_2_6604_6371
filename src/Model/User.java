/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import GUI.NewAccount_Screen;
import Restaurant_System.Restaurant;
import Restaurant_System.UserXML;
import java.awt.Component;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author nouryasser
 */
public class User {

    private static Component rootPane;
    private String name;
    private String username;
    private String password;

    public User(String name, String username, String password) {
        this.name = name;
        this.username = username;
        this.password = password;
    }

    public static User Validate(List<User> userslist, String username, String password) {
        User myuser = null;
        for (int i = 0; i < userslist.size(); i++) {
            if (userslist.get(i).getUsername().equals(username) && userslist.get(i).getPassword().equals(password)) {
                myuser = userslist.get(i);
                break;
            }
        }
        return myuser;
    }

    public static void saveUsers(Restaurant myRestaurant, UserXML user) throws JAXBException {
        List<User> userslist = null;
        JAXBContext jax = JAXBContext.newInstance(Restaurant.class);
        Marshaller m = jax.createMarshaller();
        myRestaurant.getUsers().getUserslist().add(user);
        if (user.getName().equals("John Doe") || user.getName().equals("Brian Johnes")) {
            m.marshal(myRestaurant, new File("restaurant.xml"));
        }
    }

    public static void createAccount(Restaurant myRestaurant, JTextField jTextField1, JTextField jTextField2, JTextField jTextField3, JTextField jTextField4, JTextField jTextField5) {
        UserXML user = new UserXML();
        String name = jTextField1.getText() + " " + jTextField2.getText();
        user.setName(name);
        user.setRole("Client");
        user.setUsername(jTextField3.getText());
        if (jTextField4.getText().equals(jTextField5.getText())) {
            user.setPassword(jTextField5.getText());
            try {
                saveNewAccount(myRestaurant, user);
            } catch (JAXBException ex) {
                Logger.getLogger(NewAccount_Screen.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(rootPane, "Account Created Successfully");
        } else {
            JOptionPane.showMessageDialog(rootPane, "Please Enter Matching Passwords");
        }
    }

    public static void saveNewAccount(Restaurant myRestaurant, UserXML user) throws JAXBException {
        List<User> userslist;
        JAXBContext jax = JAXBContext.newInstance(Restaurant.class);
        Marshaller m = jax.createMarshaller();
        myRestaurant.getUsers().getUserslist().add(user);
        m.marshal(myRestaurant, new File("restaurant.xml"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
